package com.example.basics;

public class Main {
    public static void main(String[] args) {
        // line comment

        /*
            block comment
        */

        /**
         *  javadoc comment
         */

        //Data Types
            /*
                -----------------------------------------------------------------
                |       bits     |    min value |   max value   |  literal form |
                -----------------------------------------------------------------
                | byte |    8    |    -128      |    127        |         0     |
                | short|    16   |    -32768    |    32768      |         0     |
                | int  |    32   |              |               |         0     |
                | long |   64    |              |               |         0L/l  |
                -----------------------------------------------------------------
                |float |    32   |              |               |      0.0f/F   |
                |double|   64    |              |               |  0.0 or 0.0d/D|
                -----------------------------------------------------------------
             */

        int i = -9;
        long l = 587900000000L;
        float f = 98.4056f;
        double d = 97.456, d1 = 89797.8989D;

        char accentedU = '\u00DA'; // Ú unicode : \ u followed by 4 digit hex value
        System.out.println(accentedU);

        // ** Primitive types are stored by value

        // Prefix and Postfix operators
        int test = 10;
        System.out.println(++test);
        System.out.println(test++);

        //Compound assignmnet operators
        // += -= /= *= %=

        //Operator precedence
        /*
        // can overwrite using ()
            post fix -> pre fix -> multiplicative -> additive
            x--         ++x         * / %            + -
         */
    }
}
