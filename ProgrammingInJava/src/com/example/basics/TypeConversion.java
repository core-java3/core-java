package com.example.basics;

public class TypeConversion {
    public static void main(String[] args) {
        // Implicit conversion - Automatically performed by the compiler - ** Widening conversions
        int intValue = 50;
        long longValue = intValue;

        //** Mixed integer sizes : Use largest integer in equation
        //** Mixed floating point sizes : Uses double
        //** Mixed integer and floating point : Uses largest floating point in equation

        //-----------------------------------------------------------------------------

        // Explicit conversion - Casting - ** Widening or narrowing conversions
        long longValue1 = 50;
        int intValue1 = (int) longValue1;
    }
}
