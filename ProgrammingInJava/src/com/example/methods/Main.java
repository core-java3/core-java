package com.example.methods;

public class Main {
    public static void main(String[] args) {
        //Parameters passed by value - Parameter receives a copy of the original value

        //Reasons a method exists -
        //1. End of method
        //2. Return statement
        //3. Error occurs

        /*
            Command line arguments - Main method acts as the entry point

         */
    }
}
