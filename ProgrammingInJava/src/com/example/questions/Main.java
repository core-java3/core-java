package com.example.questions;

public class Main {
    public static void main(String[] args) {
        int sum = 0;
        int step = 1;

        for (int i = 1; i <= 1000; i++) {
            if (step <= 5) {
                if (i % 3 == 0 && i % 5 == 0) {
                    sum += i;
                    step++;
                    System.out.println(i);
                }
            }else{
                break;
            }
        }
        System.out.println("Sum : "+sum);
    }
}
