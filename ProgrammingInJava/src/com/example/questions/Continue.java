package com.example.questions;

public class Continue {
    public static void main(String[] args) {

        int num = 3;
        int step = 0;
        int sum = 0;

        do {
            if (isEvenNumber(num)) {
                System.out.println(num);
                step++;
                sum += num;
            }
            num++;
        }
        while (step < 5);

//        while (num <= 20) {
//            num++;
//            if (step < 5) {
//                if (isEvenNumber(num)) {
//                    System.out.println(num);
//                    step++;
//                    sum+=num;
//                }
//            }
//        }
        System.out.println("Steps : " + step);
        System.out.println("Sum : " + sum);
    }

    private static boolean isEvenNumber(int numbers) {

        if (numbers % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }
}
