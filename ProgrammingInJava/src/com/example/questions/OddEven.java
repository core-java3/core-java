package com.example.questions;

public class OddEven {
    public static void main(String[] args) {

        for (int i = 1; i <= 20; i++) {
            if (i%2 == 0){
                System.out.println(i+" : This is Even");
            }else{
                System.out.println(i+" : This is Odd");
            }
        }
    }
}
