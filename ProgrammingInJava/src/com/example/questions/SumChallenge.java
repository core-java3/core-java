package com.example.questions;

public class SumChallenge {
    public static void main(String[] args) {
        System.out.println(sumDigits(16091));
    }

    private static int sumDigits(int num) {
        if (num >= 10) {
            int sum = 0;
            while (num > 0) {
                sum = sum + num % 10;
                num = num / 10;
            }
            return sum;
        }else{
            return -1;
        }
    }
}
