package com.example.strings;

public class StringBuilderTest {
    //String builders are mutable

    public static void main(String[] args) {
        String time = "9.00";
        StringBuilder sb = new StringBuilder("I flew to Florida on #175");
        int pos = sb.indexOf(" on");
        sb.insert(pos," at ");
        System.out.println(sb.toString());
    }
}
