package com.example.loops;

public class Main {
    public static void main(String[] args) {
        //calculateFactorial(4);
        testDoWhile();
    }

    private static void testDoWhile() {
        //Condition is checked at the end of the loop
        //Loop body executes at least once
        int i = 9;
        do {
            i--;
            System.out.println(i);
        } while (i > 0);
    }

    private static void calculateFactorial(int i) {
        int factorial = 1;
        while (i > 1) {
            factorial *= i;
            i--;
        }
        System.out.println(factorial);
    }
}
