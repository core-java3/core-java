package com.example.loops;

public class ParallelArray {
    public static void main(String[] args) {

        double leftValues[] = {100.0d, 25.0d, 225.0d, 11.0d};
        double rightValues[] = {50.0d, 92.0d, 17.0d, 3.0d};
        char opCode[] = {'d','a','s','m'};
        double results[] = new double[opCode.length];

        for (int i = 0; i < opCode.length; i++) {
            switch (opCode[i]){
                case 'a':
                    results[i]=leftValues[i]+rightValues[i];
                    break;
                case 's':
                    results[i]=leftValues[i]-rightValues[i];
                    break;
                case 'm':
                    results[i]=leftValues[i]*rightValues[i];
                    break;
                case 'd':
                    results[i]= rightValues[i] != 0.0 ? leftValues[i]/rightValues[i] : 0.0d;
                    break;
                default:
                    System.out.println("Invalid opCode "+opCode[i]);
                    results[i] = 0.0d;
            }
        }

        for (double re: results) {
            System.out.println(re);
        }
    }
}
