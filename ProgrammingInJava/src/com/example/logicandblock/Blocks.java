package com.example.logicandblock;

public class Blocks {
    public static void main(String[] args) {
        int i = 4;

        //Primitive types supported switch : byte, short, int, long, char

        switch (i){
            case 1:
                System.out.println("1");
                break; // Otherwise fall through to next match
            case 2:
                System.out.println("2");
                break;
            case 3:
                System.out.println("3");
                break;
            case 4:
                System.out.println("4");
                break;
            default:
                System.out.println("default");
                break;
        }
    }
}
