package com.example.logicandblock;

public class Main {
    public static void main(String[] args) {

        // Relational operators
        // > < >= <= == !=
        System.out.println('c'>'a'); // we can perform all the relational based logic with char

        //Conditional assignment
        // condition ? true-value : false-value
        int value1 = 7;
        int value2 = 5;

        System.out.println(value1>value2 ? value1 : value2);

        if (true){

        }else if(true){

        }else{

        }

        //Logical operators
        /*
            & AND
            | OR
            ^ XOR - Same type false. Different type true.
            ! Negation (NOT)
         */

        //Conditional logical operators - Right side only executes when needed
        /*
            && AND
            || OR

         */
    }
}
