package com.example.datatypes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {

        //Instant class
        //LocalTime LocalDate LocalDateTime - no time zone
        //ZonedDateTime

        //DateTimeFormatter
        LocalDate today = LocalDate.now();
        System.out.println(today);

        DateTimeFormatter usDate = DateTimeFormatter.ofPattern("MM-dd-yyyy");

        System.out.println(usDate.format(today));
    }
}
