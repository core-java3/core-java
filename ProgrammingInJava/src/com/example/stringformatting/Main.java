package com.example.stringformatting;

public class Main {
    public static void main(String[] args) {
        //Parts of a format specifier:

        // %[argument index][flags][width][precision]conversion
        /*
            Common format conversions

            1. d - Decimal format
            2. x or X - Hexadecimal format
            3. f - Floating point format
            4. e or E - Scientific notation
            5. s - String format
         */
        int val = 32;
        String s = String.format("%x",val);
        String s1 = String.format("%#x",val);
        System.out.println(s);
        System.out.println(s1);
//        20
//        0x20

        //Format flags:
        //# - Include radix
        //0 - Zero padding (
        // - - Left justify
        // , - include grouping separator
        //space - leading space when positive number
        //+ - always show sign
        //( - Enclose negative values in parenthesis

        int w = 5, x = 234, y = 67;
        String a = String.format("W:%04d X:%04d", w,x);
        String a1 = String.format("X:%04d Y:%04d", x,y);
        System.out.println(a);
        System.out.println(a1);
        //W:   5 X:   234

        String a2 = String.format("W:%-4d X:%-4d", w,x);
        String a3 = String.format("X:%-4d Y:%-4d", x,y);
        System.out.println(a2);
        System.out.println(a3);

        int iVal = 1234567;

        String a4 = String.format("%d",iVal);
        String a5 = String.format("%,d",iVal);
        System.out.println(a4);
        System.out.println(a5);

        double dVal = 1234567.0d;
        String a6 = String.format("%,.2f",dVal);
        System.out.println(a6);

        int iPosVal = 123, iNegVal = -456;
        String a7 = String.format("% d",iPosVal);
        String a8 = String.format("% d",iNegVal);
        String a9 = String.format("%+d",iPosVal);
        String a10 = String.format("%(d",iNegVal);
        System.out.println(a7);
        System.out.println(a8);
        System.out.println(a9);
        System.out.println(a10);
    }
}
