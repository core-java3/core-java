package com.example.stringformatting;

public class ArgumentIndex {
    public static void main(String[] args) {
        //Argument Index
        /*
            1. Not specified - Corresponds sequentially to argument
            2. index$ - Index of argument to use (1-based)
            3. < - Corresponds to same argument as previous format specifier

         */

        int valA = 100, valB = 200, valC = 300;

        String s = String.format("%d %d %d", valA, valB, valC);
        System.out.println(s);

        String s1 = String.format("%3$d %1$d %2$d", valA, valB, valC);
        System.out.println(s1);

        String s2 = String.format("%2d %<d %1d", valA, valB, valC);
        System.out.println(s2);
    }
}
